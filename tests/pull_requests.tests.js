var should = require("should");
var bitbucket = require('../index');
var goodOptions = require('./helper').credentials;
var repoData = require('./helper').repository;
// var issue = {title: "New Issue", content: "Issue content"};
// var component = "First component";

describe('Repository', function() {
  var repo;

  before(function (done) {
    var client = bitbucket.createClient(goodOptions);
    client.getRepository(repoData, function (err, repository) {
      if (err) {return done(err);}
      repo = repository;
      done();
    });
  });

  describe(".pullRequests()", function () {
    var componentId = null;

    describe(".getAll()", function () {
      it('should get all pullRequests', function (done) {
        repo.pullRequests().getAll(function (err, result) {
          Array.isArray(result).should.be.ok;
          done(err);
        });
      });
    });

  });
});
